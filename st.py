#!/usr/bin/env python3
'''
search with tracker
'''

import argparse
import subprocess
import os

parser = argparse.ArgumentParser()
parser.add_argument("search_term", help="Add * to search with wildcard (only works behind query term). Enclose multiple keywords with single quotes, space means AND while OR must be typed explicitly. For querying a sentence additionally enclose with escaped double quotes (e.g. '\"hello world\"') ")
parser.add_argument("-f", dest="filename", action="store_true", help="search for filename containing search_term")
parser.add_argument("-l", dest="list", help="list options. p = path, f = filename, d = access date")
parser.add_argument("-s", dest="sort", help="sort options. p = path, f = filename, d = access datee")
parser.add_argument("-p", dest="path", help="search in path, enter a path or enter: db = HOME/Dropbox")
args = parser.parse_args()

select = "nie:url(?f) "

if args.path == None:
    path = ""
elif args.path == 'db':
    home = os.getenv("HOME")
    print(home)
    path = "?f nie:url ?url FILTER(fn:starts-with(?url, 'file://" + home + "/Dropbox')) . "
else:
    path = "?f nie:url ?url FILTER(fn:starts-with(?url, 'file://%s')) . " % args.path

if args.filename:
    where = "?f nfo:fileName ?fn FILTER(fn:contains(?fn, '%s'))" % args.search_term
else:
    where = "?f fts:match '%s'" % args.search_term

if args.list:
    select = ""
    for i in args.list[::-1]:
        if i == 'd':
            select = "nfo:fileLastAccessed(?f) " + select
        if i == 'p':
            select = "nie:url(?f) " + select
        if i == 'f':
            select = "nfo:fileName(?f) " + select

if args.sort == None:
    subprocess.Popen("tracker sparql -q \"SELECT " + select + "WHERE { " + path + where + "}\"", shell=True).wait()
else:
    if args.sort == 'd':
        order = "nfo:fileLastAccessed(?f)"
    elif args.sort == 'f':
        order = "nfo:fileName(?f)"
    elif args.sort == 'p':
        order = "nie:url(?f)"
    subprocess.Popen("tracker sparql -q \"SELECT " + select + "WHERE { " + path + where + "} ORDER BY " + order + "\"", shell=True).wait()



